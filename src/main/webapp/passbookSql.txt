-- Table: passbook_model

-- DROP TABLE passbook_model;

CREATE TABLE passbook_model
(
  passbook_modelid serial NOT NULL,
  name character varying(20),
  column_width character varying(50),
  column_name character varying(250),
  header_height double precision,
  line_height double precision,
  count_rowperpage integer, -- 
  CONSTRAINT passbook_model_pkey PRIMARY KEY (passbook_modelid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE passbook_model
  OWNER TO postgres;
COMMENT ON COLUMN passbook_model.count_rowperpage IS '
';

----------------------------------------------------------------------

-- Function: passbook_data(integer, integer)

-- DROP FUNCTION passbook_data(integer, integer);

CREATE OR REPLACE FUNCTION passbook_data(accountid integer, receiptnumber integer)
  RETURNS SETOF passbook_data AS
$BODY$
declare v_accountid integer := $1;
begin
return query
select account.accountcode nomer_rekening,
       INITCAP(CONCAT(customer.name1,' ',customer.name2,' ',customer.name3,' ',customer.name4)) nama_nasabah,
       saving.savingcode kode_produk,
       saving.savingname,
       transaction.transactiondate tanggal,
       transaction.receiptnumber receiptnumber,
       transactiontype.transactiontypeid,
       transactiontype.transactiontypecode sandi,
       transactionmapping.transactionmappingname,
       case 
            when transaction.credit is false
            then transaction.amount
            else 0
       end debit,
       case 
            when transaction.credit is true
            then transaction.amount
            else 0
       end kredit,
       transaction.afterbalance saldo,
       operatoruser.username nama_petugas
from account
inner join transaction on transaction.accountid = account.accountid
inner join customer on customer.customerid = account.customerid
inner join operatoruser on operatoruser.userid = transaction.creator
inner join saving on saving.savingid = account.savingid
inner join transactionmapping on transactionmapping.transactionmappingid = transaction.transactionmappingid
inner join transactiontype on transactiontype.transactiontypeid = transactionmapping.transactiontypeid
where account.savingid is not null and account.accountid = $1 and transaction.receiptnumber  > $2
order by customer.name1,transaction.transactionid;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION passbook_data(integer, integer)
  OWNER TO postgres;

----------------------------------------------------------------------

-- Function: passbook_combobox(integer)

-- DROP FUNCTION passbook_combobox(integer);

CREATE OR REPLACE FUNCTION passbook_combobox(accountid integer)
  RETURNS SETOF passbook_combobox3 AS
$BODY$

select account.accountid,
	accountcode,receiptnumber,
	 (a.count_print % (select count_rowperpage from passbook_model inner join saving on saving.passbook_modelid=passbook_model.passbook_modelid
										inner join account on account.savingid=saving.savingid
										where account.accountid=1304)) +1 as count_line
from account
inner join transaction on transaction.accountid=account.accountid
inner join saving on saving.savingid=account.savingid
inner join passbook_model on passbook_model.passbook_modelid=saving.passbook_modelid
left join 
(
	select count(transactionid) count_print,customer from transaction where accountid=1304 and isprint is true group by customer
) a on a.customer=transaction.customer
where account.accountid=1304 and transaction.isprint is true
group by account.accountid,accountcode,receiptnumber,count_rowperpage,transaction.isprint,a.count_print
order by receiptnumber desc limit 1
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION passbook_combobox(integer)
  OWNER TO postgres;

----------------------------------------------------------------------

type ??
----------------------------------
combobox3 = 
accountid integer
accountcode character varying
receiptnumberlast integer
count_line bigint

------------------------------------
nomer_rekening character varying
nama_nasabah text
kode_produk character varying
sacingname character varying
tanggal date
receiptnumber integer
transactiontyped integer
sandi character varying
transactionmappingname character varying
debit double precision
kredit double precision
saldo double precision
nama_petugas character varying


saving + passbook_modelid
transaction + isprint
