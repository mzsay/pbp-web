/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaia.printing.pbp.web;

import com.sun.xml.internal.ws.util.StringUtils;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

/**
 *
 * @author say_arifin@yahoo.com
 */
public class Main {

    public static void main(String[] args) throws IOException {
        String isiDataPrint[][] = new String[10][8];
        int panjangPerKolom[] = {10, 50, 60, 60, 60, 70, 50, 50};

        Double qty = 1221.01;
        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        System.out.println("a"+formatter.format(qty));
        String res = String.format(Locale.ENGLISH, "%.2f", qty);
        System.out.println(res);
        
        if(true)
            return;

        for (int i = 0; i < isiDataPrint.length; i++) {
            isiDataPrint[i][0] = "2";
            isiDataPrint[i][1] = "01/02/2010";
            isiDataPrint[i][2] = "UYT";
            isiDataPrint[i][3] = "1200001";
            isiDataPrint[i][4] = "1000000";
            isiDataPrint[i][5] = "10000000";
            isiDataPrint[i][6] = "OPOS";
            isiDataPrint[i][7] = "OPOS";
        }

        PassBookPrinting passbookPrinting = new PassBookPrinting(isiDataPrint, panjangPerKolom);

        for (int i = 0; i < passbookPrinting.getPrinterNameList().length; i++) {
            System.out.println(i + ". " + passbookPrinting.getPrinterNameList()[i]);
        }
        System.out.println("");

        passbookPrinting.setPrintToFile(true);
        passbookPrinting.setLineHeight(20.0);
        passbookPrinting.setTop(100.0);
        passbookPrinting.setLeft(20.0);


        passbookPrinting.setPrintServiceName("Nitro PDF Creator 2");
//        passbookPrinting.printServiceName = "HP Color LaserJet CM1312 MFP Series PCL 6";

        passbookPrinting.StartPrinting();
    }
}
