/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaia.printing.pbp.web;

import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.attribute.standard.Destination;
import javax.print.attribute.standard.MediaSizeName;

/**
 *
 * @author mzsay
 */
public class PassBookPrinting extends ComponentPrinting implements Serializable{
    public String isiData[][];
    public int panjangPerKolom[];
    PrintService[] services;
    
    public PassBookPrinting(String data[][], int panjangPerKolom[]){
        this.isiData = data;
        this.panjangPerKolom = panjangPerKolom;
    }
    
    public String[] getPrinterNameList(){
        
        services = PrintServiceLookup.lookupPrintServices(null, null);
        String servicesList[] = new String[services.length];
        for (int i = 0; i < services.length; i++) {
            servicesList[i] = services[i].getName();
        }
        
        return  servicesList;
    }
    
    public ArrayList<String> getPrinterNameArrayList(){
        
        services = PrintServiceLookup.lookupPrintServices(null, null);
        ArrayList<String> list = new ArrayList<>();
        String servicesList[] = new String[services.length];
        for (int i = 0; i < services.length; i++) {
            servicesList[i] = services[i].getName();
            list.add(services[i].getName());
        }
        
        return  list;
    }
    
    public void StartPrinting() {
        //Proses printing
        PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
        printRequestAttributeSet.add(MediaSizeName.ISO_A4);
        printRequestAttributeSet.add(new Copies(copies));
        printRequestAttributeSet.add(sides);
        
        if(printToFile){
            try {
                printRequestAttributeSet.add(new Destination(new java.net.URI(urlOutputFile)));
            } catch (URISyntaxException ex) {
                try {
//                    urlOutputFile = "file://c:/out.ps";
                    printRequestAttributeSet.add(new Destination(new java.net.URI(urlOutputFile)));
                } catch (URISyntaxException ex1) {
                    Logger.getLogger(PassBookPrinting.class.getName()).log(Level.SEVERE, null, ex1);
                }
                Logger.getLogger(PassBookPrinting.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        PrinterJob pjob = PrinterJob.getPrinterJob();
        PageFormat pf = pjob.defaultPage();
        pf.setOrientation(pageFormatOrientasi);

        Paper paper = new Paper();
        
        paper.setImageableArea(margin_left, margin_top, paper.getWidth() - margin_left * 2, paper.getHeight() - (margin_top * 2));
        pf.setPaper(paper);

        services = PrintServiceLookup.lookupPrintServices(null, printRequestAttributeSet);
        
        //set index print service
        for (int i = 0; i < getPrinterNameList().length; i++) {
            if(getPrinterNameList()[i].equalsIgnoreCase(printServiceName)) {
                printServiceIdx = i;
            }
        }
        //        pf = pjob.pageDialog(pf);

        pjob.setPrintable(new MyPrintable(isiData, panjangPerKolom), pf);
        try {
            pjob.setPrintService(services[printServiceIdx]);
            pjob.print(printRequestAttributeSet);
        } catch (PrinterException | IndexOutOfBoundsException ex) {
            Logger.getLogger(PassBookPrinting.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
