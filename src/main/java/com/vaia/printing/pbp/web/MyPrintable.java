/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaia.printing.pbp.web;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

/**
 *
 * @author say_arifin@yahoo.com
 */

class MyPrintable extends ComponentPrinting implements Printable {
   public String isiDataPrint[][];
   public int panjangPerKolom[];
   
   public MyPrintable(String getStr[][], int panjangPerKolom[]) {
        this.isiDataPrint       = getStr;
        this.panjangPerKolom    = panjangPerKolom;
        
    }

    @Override
    public int print(Graphics g, PageFormat pf, int pageIndex) {
        if (pageIndex != 0) {
            return NO_SUCH_PAGE;
        }
        Graphics2D g2 = (Graphics2D) g;
        g2.setFont(new Font(fontType, fontStyle, fontSize));
        g2.setPaint(fontColor);
        System.out.println("line : "+LineHeight);
        int lineHeight = 0, tempLenght = 0;
        for (int i = 0; i < isiDataPrint.length; i++) {
            for (int j = 0; j < isiDataPrint[i].length; j++) {
                try{
                    tempLenght += left;
                    g2.drawString(isiDataPrint[i][j], tempLenght,  Math.round(top) + lineHeight);
                    tempLenght += panjangPerKolom[j];
                }catch(IndexOutOfBoundsException e){}
            }
            tempLenght = 0;
            lineHeight += this.LineHeight;
        }

        Rectangle2D outline = new Rectangle2D.Double(pf.getImageableX(), pf.getImageableY(), pf.getImageableWidth(), pf.getImageableHeight());
//        g2.draw(outline);
        return PAGE_EXISTS;
    }

    
}
