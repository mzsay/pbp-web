package com.vaia.printing.pbp.web.servlet;

import com.vaia.printing.pbp.web.PassBookPrinting;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author say_arifin@yahoo.com
 */
public class PrintDataServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String dummyData = "1|Thu Apr 11 13:31:42 WIT 2013|ABCDE|1000000|1000000|AND";
        
        String printerName  = request.getParameter("printerName");
        String top          = request.getParameter("top");
        String left         = request.getParameter("left");
        String lineHeight   = request.getParameter("lineHeight");
        
        System.out.println("top :"+top);
        System.out.println("left :"+left);
        System.out.println("lineheight :"+lineHeight);
        
        String data = request.getParameter("data");
        String[] dataArr = data.split("\\|\\|");    
        
        String column = request.getParameter("arrayColumn");
        String[] columnArray = column.split("\\|");
        int[] columnArrayInt = new int[columnArray.length];
        System.out.println("column :"+column);
        
        String tempSplitCount[] = dataArr[0].split("\\|");
        String isiDataPrint[][] = new String[dataArr.length][tempSplitCount.length];
        
        //proses inssert into columnArray
        for (int i = 0; i < columnArray.length; i++) {
            System.out.println(""+columnArray[i].toString());
            columnArrayInt[i] = Integer.parseInt(columnArray[i].toString());
        }
        //proses convert data into array 2D
        for (int i = 0; i < dataArr.length; i++) {
            String tempSplit[] = dataArr[i].split("\\|");
            for (int j = 0; j < tempSplit.length; j++) {
                isiDataPrint[i][j] = tempSplit[j];
            }
        }

        PassBookPrinting passbookPrinting = new PassBookPrinting(isiDataPrint, columnArrayInt);

        for (int i = 0; i < passbookPrinting.getPrinterNameList().length; i++) {
            System.out.println(i + ". " + passbookPrinting.getPrinterNameList()[i]);
        }
        System.out.println("");

        passbookPrinting.setLineHeight(Double.parseDouble(lineHeight));
        passbookPrinting.setPrintServiceName(printerName);
        passbookPrinting.setTop(Double.parseDouble(top));
        passbookPrinting.setLeft(Double.parseDouble(left));
        passbookPrinting.setPrintToFile(true);
        passbookPrinting.StartPrinting();
                
        
        //------------------------------------------------------
        //response is not important to this page
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PrintDataServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PrintDataServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
