/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaia.printing.pbp.web;

import java.awt.Color;
import java.awt.Font;
import java.awt.print.PageFormat;
import javax.print.attribute.standard.MediaSizeName;
import javax.print.attribute.standard.Sides;

/**
 *
 * @author say_arifin@yahoo.com
 */
public class ComponentPrinting {
    
    protected  static double              margin_top            = 20;
    protected  static double              margin_left           = 20;
    protected  static int                 pageFormatOrientasi   = PageFormat.PORTRAIT;
    protected  static boolean             printToFile           = false;
    protected  static String              printServiceName      = "";
    protected  static int                 printServiceIdx       = 0;
    protected  static Sides               sides                 = Sides.ONE_SIDED;
    protected  static int                 copies                = 1;
    protected  static MediaSizeName       size                  = MediaSizeName.ISO_A4;
    protected  static String              urlOutputFile         = "file:///home/mzsay/out.ps";
    protected  static Double              LineHeight            = 20.0;
    protected  static String              fontType              = "Serif";
    protected  static int                 fontSize              = 12;
    protected  static int                 fontStyle             = Font.PLAIN;
    protected  static Color               fontColor             = Color.BLACK;
    protected  static Double              top                   = 50.0;
    protected  static Double              left                  = 10.0;
    

    public Double getLineHeight() {
        return LineHeight;
    }

    public void setLineHeight(Double LineHeight) {
        this.LineHeight = LineHeight;
    }

    public String getFontType() {
        return fontType;
    }

    public void setFontType(String fontType) {
        this.fontType = fontType;
    }

    public Double getTop() {
        return top;
    }

    public void setTop(Double top) {
        ComponentPrinting.top = top;
    }

    public Double getLeft() {
        return left;
    }

    public void setLeft(Double left) {
        ComponentPrinting.left = left;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fontColor) {
        this.fontColor = fontColor;
    }
    
    public double getMargin_top() {
        return margin_top;
    }

    public void setMargin_top(double margin_top) {
        this.margin_top = margin_top;
    }

    public static double getMargin_left() {
        return margin_left;
    }

    public static void setMargin_left(double margin_left) {
        ComponentPrinting.margin_left = margin_left;
    }

    public int getPageFormatOrientasi() {
        return pageFormatOrientasi;
    }

    public void setPageFormatOrientasi(int pageFormatOrientasi) {
        this.pageFormatOrientasi = pageFormatOrientasi;
    }

    public boolean isPrintToFile() {
        return printToFile;
    }

    public void setPrintToFile(boolean printToFile) {
        this.printToFile = printToFile;
    }

    public String getPrintServiceName() {
        return printServiceName;
    }

    public void setPrintServiceName(String printServiceName) {
        this.printServiceName = printServiceName;
    }

    public Sides getSides() {
        return sides;
    }

    public void setSides(Sides sides) {
        this.sides = sides;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public MediaSizeName getSize() {
        return size;
    }

    public void setSize(MediaSizeName size) {
        this.size = size;
    }

    public String getUrlOutputFile() {
        return urlOutputFile;
    }

    public void setUrlOutputFile(String urlOutputFile) {
        this.urlOutputFile = urlOutputFile;
    }


}
